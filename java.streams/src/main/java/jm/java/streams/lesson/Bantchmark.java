package jm.java.streams.lesson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class Bantchmark {

    public static void main(String[] args) {
        Random rand = new Random();
        List<String> data = rand
                .ints()
                .filter(i -> i>=0)
                .limit(500_000)
                .mapToObj(i -> Integer.toString(i, 16))
                .collect(Collectors.toList());

//        Map<String, Long> result = new HashMap<>();
//        for (String s: data) {
//            String start = s.substring(0,1);
//            long sum = result.getOrDefault(start, 0l);
//            result.put(start, sum + 1);
//        }

        Map<String, Long> result= data.stream()
                .collect(Collectors.groupingBy(s -> s.substring(0,1), Collectors.counting()));


        result.entrySet()
                .forEach(e -> System.out.println(e.getKey() + ">" + e.getValue()));
    }
}

package jm.java.streams.lesson;

import java.nio.file.OpenOption;
import java.util.Optional;
import java.util.Random;

public class OptionDemo {

    public static void main(String[] args) {
        Random rand = new Random();
        Optional<String> result = rand.ints(1000)
                .mapToObj(i -> Integer.toString(i, 16))
                .filter(i -> i.startsWith("A"))
                .findAny();


        System.out.println(result.isPresent());
        //System.out.println(result.get());
        System.out.println(result.orElse("NONE"));
        System.out.println(result.map(s -> s.toUpperCase()));
        System.out.println(result.map(String::toUpperCase));
        result.ifPresent(s -> System.out.println(s));

        System.out.println(result.orElseThrow(IllegalArgumentException::new));


    }
}

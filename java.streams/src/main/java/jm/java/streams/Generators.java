package jm.java.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.IntConsumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Generators {

    public static void main(String[] args) {

        Random r = new Random();
        //r.ints(100).forEach(value -> System.out.println(value));
        IntStream.range(10,20).forEach(value -> System.out.println(value));
        Stream.of("A","B","C","B").forEach(value -> System.out.println(value));

        List<String> list = Arrays.asList("A","BC","DEF","GH");

        list.stream()
                .mapToInt(s -> s.hashCode())
                .filter(i -> i < 67)
                .forEach(value -> System.out.println(value));

        list.stream()
                .map(s -> s.toLowerCase())
                .forEach(value -> System.out.println(value));

        Arrays.stream("XYZ".split(""))
                .forEach(value -> System.out.println(value));

        list.stream()
                .flatMap(s -> Arrays.stream(s.split("")))
                .forEach(value -> System.out.println(value));

        list.stream()
                .flatMapToInt(s -> s.chars())
                .forEach(value -> System.out.println(value));

        Stream.iterate("A", a -> a + "A")
                .limit(10)
                .skip(5)
                .peek(v -> System.out.println("PEEK> " + v))
                .forEach(value -> System.out.println(value));

        List<String> sList = list.stream()
                .filter(s -> s.length() < 3)
                .collect(Collectors.toList());
        System.out.println(sList);

        String msg = list.stream()
                .filter(s -> s.length() < 3)
                .collect(Collectors.joining(",", "<!--", "-->"));
        System.out.println(msg);
     }
}

package jm.java.generics.exercise

import spock.lang.Specification

class MainTest extends Specification {
    def "Otesovani ze zakaznik nasedl do spravneho auta"() {
        when:
        def msg = Exercise.nasedni(1, 2)

        then:
        msg == "Zakaznik Jana nasedl do auta Osobni."
    }
}

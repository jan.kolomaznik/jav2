package cz.ictpro._kolekce;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class DemoNullMap {

    public static void main(String[] args) {
        Map map = new HashMap();
        map.put(null, 1);

        System.out.println(map.get(null));
    }
}

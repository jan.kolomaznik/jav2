package cz.ictpro._kolekce;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Coordinate {

    private final int x, y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return x == that.x &&
                y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public static void main(String[] args) {
        Coordinate c1 = new Coordinate(1,2);
        Coordinate c2 = new Coordinate(1,2);


        Set set = new HashSet();
        set.add(c1);
        set.add(c2);
        System.out.println(set);
    }
}

package cz.ictpro._generika;

import java.util.Arrays;
import java.util.List;

public class CastDemo {

    public static double suma(List<? extends Number> cisla) {
        double result = 0;
        for (Number e : cisla) {
            result += e.doubleValue();
        }
        return result;
    }

    public static void main(String[] args) {
        Number a = 1;
        Number b = 1.5;

        List<Integer> cisla = Arrays.asList(1,2,3);
        System.out.println(suma(cisla));
    }

}

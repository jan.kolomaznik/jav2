package cz.ictpro._generika;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenerickeMetody {

    public static <T, S extends T> void copy(List<T> dest, List<S> source) {
        for (S o:source) {
            dest.add(o);
        }
    }


    public static void main(String[] args) {
        List<Number> dest = new ArrayList<>(Arrays.asList(1,2,3));
        List<Integer> source = new ArrayList<>(Arrays.asList(4, 5, 5));
        copy(dest, source);
        System.out.println(dest);
    }
}

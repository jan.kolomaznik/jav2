package cz.ictpro.factorio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Assembler extends Storage {

    //private Source[] sources;
    //private List sources;
    private List<Source> sources;

    public Assembler(String type, Source ... sources) {
        super(type);
        this.sources = Arrays.asList(sources);
    }

    public void act() {
//        for (Object o: sources) {
//            Source source = (Source) o;
//            if (!source.isAvailable()) {
//               return; // TODO
//            }
//        }
//        this.items[0].amount++;
        for (Source source: sources) {
            if (!source.isAvailable()) {
                return;
            }
        }
        int amount = items.get(getType());
        items.put(getType(), amount++);
    }


}

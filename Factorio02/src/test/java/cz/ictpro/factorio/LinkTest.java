package cz.ictpro.factorio;

import org.junit.Test;

public class LinkTest {

    static class MockSource implements Source {

        private String type;
        private boolean available;

        public MockSource(String type, boolean available) {
            this.type = type;
            this.available = available;
        }

        @Override
        public String getType() {
            return type;
        }

        @Override
        public boolean isAvailable() {
            return available;
        }
    }

    static class MockConsumer implements Consumer {
        private String[] materials;

        public MockConsumer(String ... materials) {
            this.materials = materials;
        }

        @Override
        public String[] getMaterials() {
            return materials;
        }
    }

    @Test
    public void CreateValidConnection() {
        Source source = new MockSource("Papir", true);
        Consumer consumer = new MockConsumer(new String[] {"Papir", "Tuzka", "Lepidlo"});
        // when
        new Link(source, consumer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void CreateInvalidConnection() {
        Source source = new MockSource("Papir", true);
        Consumer consumer = new MockConsumer("Beton", "Ocel");
        // when
        new Link(source, consumer);
    }

}
package cz.ictpro._enums;

import java.util.Arrays;

public class MyEnum {

    public static abstract class BarvyC {

        private static final BarvyC RED = new BarvyC(255, 0, 0) {
            @Override
            public String getColorName() {
                return "Cervena";
            }
        };
        private static final BarvyC GREEN = new BarvyC(0, 255, 0) {
            @Override
            public String getColorName() {
                return "Zelena";
            }
        };
        private static final BarvyC BLUE = new BarvyC(0, 0, 255) {
            @Override
            public String getColorName() {
                return "Modra";
            }
        };

        public final int r,g,b;

        private BarvyC(int r, int g, int b) {
            this.r = r;
            this.b = b;
            this.g = g;
        }

        public abstract String getColorName();

        @Override
        public String toString() {
            return "Barva " + Integer.toString(r, 16) + "-" +
                    Integer.toString(g, 16) + "-" +
                    Integer.toString(b, 16);
        }
    }

    public enum BarvyE {
        RED(255, 0, 0) {
            @Override
            public String getColorName() {
                return "Cervena";
            }
        },
        GREEN(0, 255, 0){
            @Override
            public String getColorName() {
                return "Zelena";
            }
        },
        BLUE(0, 0, 255){
            @Override
            public String getColorName() {
                return "Modra";
            }
        };

        public final int r,g,b;

        BarvyE(int r, int g, int b) {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        public abstract String getColorName();

        @Override
        public String toString() {
            return "Barva " + Integer.toString(r, 16) + "-" +
                    Integer.toString(g, 16) + "-" +
                    Integer.toString(b, 16);
        }
    }

    public static void main(String[] args) {
        System.out.println(BarvyC.GREEN);
        System.out.println(BarvyE.RED);
        System.out.println(Arrays.toString(BarvyE.values()));
        System.out.println(BarvyE.valueOf("RED"));
        System.out.println(BarvyE.GREEN.ordinal());
        System.out.println(BarvyE.GREEN.getColorName());
    }

}

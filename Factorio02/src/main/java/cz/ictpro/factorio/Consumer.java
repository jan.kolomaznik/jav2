package cz.ictpro.factorio;

/**
 * Vyzaduje urcity typ materialu
 * */
public interface Consumer {

    /**
     * Typy materialu, ktery customer vyzaduje
     * @return
     */
    String[] getMaterials();
}

package cz.ictpro.factorio;

/**
 * Produkce urciteho typu materialu
 */
public interface Source<T> {

    /**
     * Vraci typ materialu, ktery zdroj produkuje.
     * @return name of material.
     */
    String getType();

    /**
     * Is material available or source is depleted.
     * @return <code>true</code> is has any material.
     */
    boolean isAvailable();

}

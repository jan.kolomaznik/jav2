package cz.ictpro.factorio;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Storage<T> implements Source<T>, Consumer {

//    static protected class Item {
//        public final String material;
//        public int amount = 0;
//
//        public Item(String material) {
//            this.material = material;
//        }
//    }
//
//    protected Item[] items = new Item[100];
    //protected Map items = new HashMap();
    protected Map<String, Integer> items = new HashMap();
    private Class<T> type;

    public Storage(Class<T> type) {
        items.put(type.getName(), 0);
        this.type = type;
    }

    @Override
    public String[] getMaterials() {
//        int length = 0;
//        for(Item item: items) {
//            if (item != null) {
//                length++;
//            }
//        }
//        String[] result = new String[length];
//        int i = 0;
//        for(Item item: items) {
//            if (item != null) {
//                result[i++] = item.material;
//            }
//        }
//        return result;
        Set<String> keys = items.keySet();
        String[] result = new String[keys.size()];
        int i = 0;
        for (String key: keys) {
            result[i++] = key;
        }
        return result;
    }

    @Override
    public String getType() {
        //return items[0].material;
        return type.getName();
    }

    @Override
    public boolean isAvailable() {
        //return items[0].amount > 0;
        return (int)items.get(type) > 0;
    }
}

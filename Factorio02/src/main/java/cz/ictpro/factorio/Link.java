package cz.ictpro.factorio;


import java.util.Objects;

/**
 * Spojeneni mezi {@link Consumer} a {@link Source}.
 */
public class Link<T> {

    private Consumer consumer;
    private Source<T> source;

    public Link(Source<T> source, Consumer consumer) {
        this.consumer = consumer;
        this.source = source;
        isValidConnection();
    }

    /**
     * {@link Source} poskytuje jden z materialu,
     * ktery {@link Consumer} vyzaduje.
     */
    private void isValidConnection() {
        //throw new UnsupportedOperationException("Not implemented yet");
        for(String material: consumer.getMaterials()) {
//            if (source.getType() == material) { // spoleham na reference
//                return;
//            }
//              if (material.equals(source.getType())) { // spoleham na to, ze to neni null
//                 return;
//              }
            if (Objects.equals(source.getType(), material)) {
                return;
            }
        }
        throw new IllegalArgumentException("Source not provide any requirement material.");
    }
}

package cz.ictpro._function;

public class DefaultMethod {

    public interface Pozice {

        double getX();
        double getY();
        default double size() {
            return Math.sqrt(getX() * getX() + getY() * getY());
        }
    }

    public static class SimplePositon implements Pozice {

        private double x, y;

        public SimplePositon(double x, double y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public double getX() {
            return x;
        }

        public void setX(double x) {
            this.x = x;
        }

        @Override
        public double getY() {
            return y;
        }

        public void setY(double y) {
            this.y = y;
        }
    }

    public static class StringPosition implements Pozice {
        private final String data;

        public StringPosition(String data) {
            this.data = data;
        }

        @Override
        public double getX() {
            return Double.parseDouble(data.split(":")[0]);
        }

        @Override
        public double getY() {
            return Double.parseDouble(data.split(":")[1]);
        }
    }

    public static void main(String[] args) {
        Pozice sp = new SimplePositon(1.5, 2.8);
        System.out.println(sp.size());

        Pozice strp = new StringPosition("1:5");
        System.out.println(strp.size());
    }
}

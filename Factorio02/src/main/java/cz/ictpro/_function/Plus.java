package cz.ictpro._function;

import java.util.Map;
import java.util.function.DoubleBinaryOperator;

public class Plus implements DoubleBinaryOperator {
    @Override
    public double applyAsDouble(double left, double right) {
        return left + right;
    }

    public static void calc(double a, DoubleBinaryOperator op, double b) {
        System.out.println(op.applyAsDouble(a, b));
    }

    public static void main(String[] args) {
        Plus plus = new Plus();
        calc(1, plus, 2);
        calc(5, plus, 4);
        calc(5, new DoubleBinaryOperator() {
            @Override
            public double applyAsDouble(double left, double right) {
                return left - right;
            }
        }, 4);
        calc(15, new DoubleBinaryOperator() {
            @Override
            public double applyAsDouble(double left, double right) {
                return Math.max(left, right);
            }
        }, 20);
    }
}

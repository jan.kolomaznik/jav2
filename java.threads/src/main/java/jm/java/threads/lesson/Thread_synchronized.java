package jm.java.threads.lesson;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Thread_synchronized {

    private int x,y;

    private final ReadWriteLock lock4xy = new ReentrantReadWriteLock();

    public Thread_synchronized(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int[] get() {
        //synchronized (this) {
        lock4xy.readLock().lock();
        System.out.println("get: " + Thread.currentThread().getName());
        int[] result = new int[]{x, y};
        lock4xy.readLock().unlock();
        return result;

        //}
    }

    public void set(int x, int y) {
        //synchronized (this) {
        lock4xy.writeLock().lock();
        System.out.println("set: " + Thread.currentThread().getName());
        this.x = x;
        this.y = y;
        lock4xy.writeLock().unlock();
        //}
    }

    public static void main(String[] args) {
        Random random = new Random();
        Thread_synchronized p = new Thread_synchronized(0,0);
        Runnable r = () -> {
            String name = Thread.currentThread().getName();
            while (true) {
                int x = random.nextInt(10);
                int y = random.nextInt(10);

                //synchronized (p) {
                    System.out.println();
                    p.set(x, y);
                    Thread.yield();
                    int[] xy = p.get();
                    System.out.printf("%s: [%d, %d] -> %s%n", name, x, y, Arrays.toString(xy));
                //}
            }
        };

        new Thread(r, "Vlakni 1").start();
        new Thread(r, "Vlakni 2").start();
    }
}

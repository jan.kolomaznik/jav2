package jm.java.threads.lesson;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Thread_notify extends Thread {

    private final Object monitor = new Object();

    private final Semaphore lock = new Semaphore(5);

    private int value = -1;

    public Thread_notify(String jmeno) {
        super(jmeno);
    }

    public void run() {
        for (int i = 1; i <= 3; i++) {
            System.out.println(i + ". " + getName());
            value = i;
            synchronized (monitor) {
                monitor.notifyAll();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Jsem vzhuru - " + getName());
                return;
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread_notify vl = new Thread_notify("Pepa");
        new Thread(() -> {
            while (!interrupted()) {
                System.out.println("Cyklus");
                try {
                    synchronized (vl.monitor) {
                        vl.monitor.wait(100);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
                System.out.println(Thread.currentThread().getName() + ":" + vl.value);
            }
        }).start();
        new Thread(() -> {
            while (!interrupted()) {
                try {
                    synchronized (vl.monitor) {
                        vl.monitor.wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
                System.out.println(Thread.currentThread().getName() + ":" + vl.value);
            }
        }).start();
        new Thread(() -> {
            while (!interrupted()) {
                try {
                    synchronized (vl.monitor) {
                        vl.monitor.wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
                System.out.println(Thread.currentThread().getName() + ":" + vl.value);
            }
        });//.start();
        vl.start();
        Thread.sleep(2500);
        vl.interrupt();
    }
}
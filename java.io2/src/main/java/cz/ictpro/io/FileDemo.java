package cz.ictpro.io;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileDemo {

    public static void main(String[] args) {
        try(BufferedReader br =
                    new BufferedReader(
                            new FileReader(
                                    "build.gradle"
                            )
                    )
        ) {

            //br.lines().forEach(System.out::println);

            String line;
            while((line = br.readLine()) != null) {
                System.out.println(line);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

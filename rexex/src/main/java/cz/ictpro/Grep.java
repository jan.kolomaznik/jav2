package cz.ictpro;

import java.io.*;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class Grep {

    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("regex file");
            return;
        }

        Pattern pattern = null;
        try {
            pattern = Pattern.compile(args[0]);
        } catch (PatternSyntaxException e) {
            System.out.println("Invalid regex syntax");
            return;
        }

        File file = new File(args[1]);
        if (!file.exists()) {
            System.out.println("File not exists");
            return;
        }

        if (file.isFile()) {
            grepFile(file, pattern);
        } else {
            grepDir(file, pattern);
        }
    }

    public static void grepDir(File dir, Pattern pattern) {
        Arrays.stream(dir.listFiles())
            .forEach(f -> {
                try {
                    if (f.isFile()) {
                        grepFile(f, pattern);
                    } else if (f.isDirectory()) {
                        grepDir(f, pattern);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

    }

    public static void grepFile(File file, Pattern pattern) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            br.lines()
                    .filter(pattern.asPredicate())
                    .forEach(System.out::println);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

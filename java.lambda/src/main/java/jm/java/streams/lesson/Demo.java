package jm.java.streams.lesson;

public class Demo {

    @FunctionalInterface
    interface MathOp {
        double execute(double a, double b);
        default double inverse(double a, double b) {
            return execute(b, a);
        }
    }

    public static void use(MathOp ... ops) {
        for (MathOp op: ops) {
            System.out.println(op.execute(2.0,3));
            System.out.println(op.inverse(2.0,3));
        }

    }

    public static void main(String[] args) {
        MathOp plus = (a, b)  -> a + b;
        MathOp pow = Math::pow;
        MathOp pow2 = (a, b) -> Math.pow(a, b);

        use(plus);
        use(pow);
        use(plus, pow, pow2);


    }


}

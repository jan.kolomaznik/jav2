package jm.java.streams.lesson;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DemoFrame {
    private JPanel root;
    private JButton button1;
    private JButton button2;

    public DemoFrame() {
        button1.addActionListener(this::doOpenFile);
        button2.addActionListener(this::doOpenFile);
    }

    public void doOpenFile(ActionEvent e) {
        System.out.println(e.getActionCommand());
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("DemoFrame");
        frame.setContentPane(new DemoFrame().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

import bh.greenfoot.runner.GreenfootRunner;
import cz.mendelu.pjj.game2048.greenfoot.Game2048World;

/**
 * A sample runner for a greenfoot project.
 */
public class Runner extends GreenfootRunner {
    static {
        // 2. Bootstrap the runner class.
        bootstrap(Runner.class,
                // 3. Prepare the configuration for the runner based on the world class
                Configuration.forWorld(Game2048World.class)
                        // Set the project name as you wish
                        .projectName("Game 2048")
                        .hideControls(true)
        );
    }
}
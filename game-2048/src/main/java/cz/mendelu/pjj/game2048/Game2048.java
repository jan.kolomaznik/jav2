package cz.mendelu.pjj.game2048;


import java.util.Random;

public class Game2048 {

    private final int[][] pole;
    private final int size;

    public Game2048(int size) {
        this.pole = new int[size][size];
        this.size = size;
        this.addNewNumber();
    }

    public int get(int x, int y) {
        return pole[x][y];
    }

    private Random rand = new Random();

    public void addNewNumber() {
        System.out.println("addNewNumber");
        int x = rand.nextInt(size);
        int y = rand.nextInt(size);
        if (pole[x][y] == 0) {
            pole[x][y] = 2;
        } else {
            throw new AddNewNumberException();
        }
        // Pokud číslo nejde pridat čísla (všechna pole jsou plná), pak vyvolejte kontrolovanou výjimku AddNewNumberException.
    }

    public boolean moveLeft() {
        System.out.println("moveLeft");
        return false;
    }

    public boolean moveRight() {
        System.out.println("moveRight");
        return false;
    }

    public boolean moveUp() {
        System.out.println("moveUp");
        return true;
    }

    public boolean moveDown() {
        System.out.println("moveDown");
        return false;
    }
}

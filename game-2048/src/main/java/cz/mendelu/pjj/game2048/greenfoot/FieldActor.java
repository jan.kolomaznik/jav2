package cz.mendelu.pjj.game2048.greenfoot;

import greenfoot.Actor;
import greenfoot.GreenfootImage;
import greenfoot.World;

import java.awt.*;

public class FieldActor extends Actor {

    private static final float ONE = Game2048World.SIZE * Game2048World.SIZE;
    private static final Font FONT = new Font(Font.MONOSPACED, Font.BOLD, 64);
    private static final int MARGIN = 5;

    public void setValue(int value) {
        GreenfootImage image = new GreenfootImage(Game2048World.CELL, Game2048World.CELL);

        // Calculate nice color for background
        Color color = Color.LIGHT_GRAY;
        if (value > 1) {
            float base = Integer.SIZE - Integer.numberOfLeadingZeros(value);
            float c = base / ONE;
            color = new Color(1f - c, 1f, c);
        }

        // Draw background rectangle
        image.setColor(color);
        image.fillRect(MARGIN, MARGIN, Game2048World.CELL - (MARGIN * 2), Game2048World.CELL - (MARGIN * 2));

        // If not 0 draw Number
        if (value != 0) {
            image.setColor(Color.BLACK);
            String number = Integer.toString(value);
            image.setFont(FONT);
            int x = Game2048World.CELL / 2 - (number.length() * 19);
            int y = Game2048World.CELL / 2 + 26;
            image.drawString(number, x, y);
        }
        setImage(image);
    }
}

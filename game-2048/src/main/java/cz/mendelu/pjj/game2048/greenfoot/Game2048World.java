package cz.mendelu.pjj.game2048.greenfoot;

import cz.mendelu.pjj.game2048.Game2048;
import greenfoot.Actor;
import greenfoot.Greenfoot;
import greenfoot.World;

public class Game2048World extends World {

    static final int SIZE = 4;
    static final int CELL = 200;

    private Game2048 game2048 = new Game2048(SIZE);

    private FieldActor[][] fields = new FieldActor[SIZE][SIZE];

    public Game2048World() {
        super(SIZE, SIZE, CELL);
        for (int x = 0; x < SIZE ; x++) {
            for (int y = 0; y < SIZE; y++) {
                fields[x][y] = new FieldActor();
                addObject(fields[x][y], x, y);
            }
        }
        update();
    }

    @Override
    public void act() {
        String key = Greenfoot.getKey();
        if (key != null) {
            boolean valid = false;
            if (key == "left") {
                valid = game2048.moveLeft();
            } else if (key == "right") {
                valid = game2048.moveRight();
            } else if (key == "up") {
                valid = game2048.moveUp();
            } else if (key == "down") {
                valid = game2048.moveDown();
            }

            if (valid == true) {
                try {
                    game2048.addNewNumber();
                    update();
                } catch (RuntimeException e) {
                    removeObjects(getObjects(null));
                    setBackground("images/game_over.png");
                    Greenfoot.stop();
                }

            }
        }
    }

    private void update() {
        for (int x = 0; x < SIZE ; x++) {
            for (int y = 0; y < SIZE; y++) {
                fields[x][y].setValue(game2048.get(x, y));
            }
        }
    }
}

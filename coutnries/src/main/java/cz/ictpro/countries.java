package cz.ictpro;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.HttpsURLConnection;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class countries {

    public static void main(String[] args) throws Exception {
        URL url = new URL("https://restcountries.eu/rest/v2/lang/en");
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

        System.out.println("ResponseCode:" + connection.getResponseCode());

        JSONParser parser = new JSONParser();

        try(Reader r = new InputStreamReader(connection.getInputStream())) {
            JSONArray countries = (JSONArray) parser.parse(r);
            countries.stream()
                    .map(c -> ((JSONObject)c).get("nativeName"))
                    .forEach(System.out::println);
        }
    }
}

package cz.ictpro;

import java.io.*;
import java.net.Socket;

public class TCPHTTP {

    public static void main(String[] args) throws IOException {
        Socket sock = new Socket("https://www.ictpro.cz/", 80); // připojení

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
        BufferedReader br = new BufferedReader(new InputStreamReader(sock.getInputStream()));

        bw.write("GET / HTTP/1.1\n");  // zapíšeme předem připravený požadavek
        bw.flush(); // vyprázdnění (tzn. odeslání) bufferu

        String line = br.readLine();

// dokud jsou data, opakuj
        while (line != null) {
            System.out.println(line);  // platná data vypisuj
            line = br.readLine();
        }

        sock.close(); // zavření socketu

    }
}

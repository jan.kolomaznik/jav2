JAVA - POKROČILÉ TECHNIKY PROGRAMOVÁNÍ  
====================================== 
 
- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Kurz je určen programátorům, kteří ovládají základy jazyka JAVA.  
Prohloubí si v něm znalosti v oblasti programování složitějších aplikací za pomoci nástrojů, jako jsou vláknové procesy, tvorba GUI.  
Účastnící se také naučí vytvářet aplikace komunikující v sítích TCP/IP a také se naučí komunikovat s databázemi a XML pomocí jazyka JAVA. 
 
Osnova kurzu 
------------ 
 
- **[Generika](/topic/java.generika)** 
  - Princip, vytváření generických tříd, funkcí 
  - Specializace tříd a funkcí, implicitní a explicitní 
- **[Java Collections API](/topic/java.collections)** 
  - Standardní kolekce List, Map, Set, Collection - vztah rozhraní. 
  - Implementace různých druhů standardních kolekcí, výhody a nevýhody 
  - Konverze standardního jazykového pole na kolekci a obráceně 
- **[Lambda výrazy](/topic/java.lambda)**
- **[Thready, asynchronní operace, synchronizace](/topic/java.threads)** 
  - [Pokročilá synchronizace](/topic/java.concurrency)
- **Základní úvod do tvorby GUI v Javě – Swing** 
- **I/O operace, práce se soubory** 
  - [Princip InputStream, OutputStream, Reader, Writer](/topic/java.io)
  - Práce se soubory a jejich implementacemi proudů, čtení a zápis binárního a textového souboru 
  - [Práce s URL a http spojením, stahování souborů pomocí HTTP](/topic/java.net.http) 
  - [Implementace jednoduchého TCP/IP klienta](/topic/java.net.tcp-ip) 
- **[Základní práce s databázemi v Javě – JDBC](/topic/java.db)** 
- **[Regulární výrazy](/topic/java.regex)** 
  - [Manipulace s metadaty souboru a cesty](/topic/java.io.file) 
- **[Síťové operace](/topic/java.net)** 
- **Práce s XML** 
  - Princip a práce s DOM 
  - Parsování pomocí Xpath 
